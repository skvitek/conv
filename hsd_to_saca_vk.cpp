/*
* HSD 2.5 to SACA converter
* with baryons and mesons 
* + baryonic resonances decay
* 
* v24.01.14
* 
* This program is a translation of original
* Elena Bratkovskaya's FORTRAN converter.
* 
* 
* Changes from original version:
* 
* 1. Work with weighted impact parameter, 
* both IBweight_MC=1 or 0 in main.f
* 
* 2. Does not need a separate input-file
* 
* 3. Work only with one timestep
* 
* 
* Input files:
*   fort.305 - baryons
*   fort.306 - mesons
* 
* Output files:
*   bdec.dat - decay data
*   saca_vk_BDEC.dat - file for SACA input
* 
* 
* Format of saca_vk_BDEC.dat
*   
* ----- header
* "  %4d  %4d  %4d  %4d  %8.2f"
* "  %4d  %4d  %6.2f  %8.2f  %16d"
* "  %8.3f  %8.3f  %8.4f  %8.4f  %8.5f  %8.5f  %8.6f  %8.6f  %8.6f"
* tA tZ pA pZ pE
* NEVENTS NtSACA DT tSACA SEED
* df VY arang alpha beta gamma a1 a2 a3  <- all 0 or 0.0
* 
* ----- body
* " %9.4f %15d %15d %15d"
* " %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %3d %12.4E"
* B RUN DT NPART
* x y z px py pz en IDSACA 1.
*
* 
* Format of bdec.dat
* *----------------------------*
* IB: id1 id2 amass px py pz
* B: out.id1 out.id2 out.amass out.px out.py out.pz  
* M: out.id1m out.id2m out.amassm out.pxm out.pym out.pzm  
* *----------------------------*
* 
* 
* Compilation: 
* 
* g++ hsd_to_saca_vk.cpp -std=c++11 \
*		-D_FILE_OFFSET_BITS=64 -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64 \
*   -D_GNU_SOURCE -D_LARGEFILE64_SOURCE -o vk_hsdsaca
* 
* or: g++ hsd_to_saca_vk.cpp -std=c++11 -o vk_hsdsaca  
*   
* 
* 24.01.2014 :
* + some new mesons  
* 
* 21.01.2014 :
* rough fix for baryonic decay with amass == 0.938 
* (2 -1 0.938 ... ... ...) => (1 1 0.938 ... ... ...)
* 
* 
* 
*/

// for B with fixed step (Bmax - Bmin) / Bstep
// just set NUM_B to 1 for weighed B
#define NUM_B 5

#include <sstream>
#include <fstream>
#include <iostream>
#include <ios>
#include <vector>
#include <cmath>
#include <time.h> 
#include <random>

using namespace std;
using std::cout;
using std::endl;

#define PI 3.141592654 
#define RMASS 0.938 
#define PMASS 0.138 
#define EMASS 0.549 

struct a4vec{
  double ax;
  double ay;
  double az;
  double a0;
};


struct a62vec{
  short id1;
  short id2;
  double amass;
  double px;
  double py;
  double pz;
  short id1m;
  short id2m;
  double amassm;
  double pxm;
  double pym;
  double pzm;
};

ofstream ofile("bdec.dat"); // file for decay data


short particle_id(short id1, short id2, bool is_b){
  short IDSACA;
  if(is_b)
  {
    switch (id1) {
      case 1:
        if(id2 == 0) IDSACA = 0; // n 
        if(id2 == 1) IDSACA = 1; // p 
      break;
/*      case 2:
        if(id2 == -1) IDSACA = 10; // Delta-
        if(id2 == 0) IDSACA = 11; // Delta0
        if(id2 == 1) IDSACA = 12; // Delta+
        if(id2 == 2) IDSACA = 13; // Delta++
      break;
*/
      case 5:
        if(id2 == 0) IDSACA = 17; // Lambda0
      break;
      case 6:
        if(id2 == -1) IDSACA = 18; // Sigma-
        if(id2 == 0) IDSACA = 19; // Sigma0
        if(id2 == 1) IDSACA = 20; // Sigma+
      break;
      default:
        IDSACA = 99;
      break;
    }
  }
  else
  {
    switch (id1) {
      case 1:
        if(id2 == -1) IDSACA= 14; // pi- 
        if(id2 == 0) IDSACA= 15; // pi0 
        if(id2 == 1) IDSACA= 16; // pi+ 
      break;
      case 2:
        if(id2 == 0) IDSACA= 29; // eta
        if(id2 == 1) IDSACA= 23; // K+
      break;
      case 3:
        if(id2 == -1) IDSACA= 21; // K-
        if(id2 == 1) IDSACA= 23; // K+
      break;
      case 5:
        if(id2 == -1) IDSACA= 30; // rho-
        if(id2 == 0) IDSACA= 31; // rho0
        if(id2 == 1) IDSACA= 32; // rho+
      break;
      case 6:
        if(id2 == 0) IDSACA= 33; // omega
      break;
      case 7:
        if(id2 == 0) IDSACA= 34; // phi
      break;
      case 8:
        if(id2 == 0) IDSACA= 35; // eta'
      break;
      case 9:
        if(id2 == -1) IDSACA= 36; // a1-
        if(id2 == 1) IDSACA= 37; // a1+
      break;
      case 11:
        if(id2 == 0) IDSACA= 22; // K0
      break;
      case 12:
        if(id2 == 0) IDSACA= 24; // K0bar
      break;
      default:
        IDSACA= 88;
      break;
    }
  }
  
  return IDSACA;
}

a4vec lorentz(double BX,double BY,double BZ,double A0,double AX,double AY,double AZ)
{
  double BS, G, BFACTOR, BS2;
  BS = pow(BX,2) + pow(BY,2) + pow(BZ,2);
  BS2 = 1.0 - BS;
  if(BS2 <= 1E-10) BS2 = 1.E-10;
  G=1.0/sqrt(BS2);
  if( BS < 0.00000001) BFACTOR=0.0;
  else BFACTOR=(G-1.0)/BS*(BX*AX+BY*AY+BZ*AZ);

  a4vec vec;
  vec.ax=AX+BFACTOR*BX-G*A0*BX;
  vec.ay=AY+BFACTOR*BY-G*A0*BY;
  vec.az=AZ+BFACTOR*BZ-G*A0*BZ;
  vec.a0=G*(A0-(BX*AX+BY*AY+BZ*AZ));

  return vec;
}

a62vec bdec(short id1, short id2, double amass, double px, double py, double pz)
{
  ofile<<"*----------------------------*"<<endl;
  ofile<<"IB: "<<id1 <<" "<<id2 <<" "<<amass <<" "<<px <<" "<<py <<" "<<pz  <<endl;
//  if (id1 ==2 && id2 ==-1 && amass < 0.939 && amass > 0.937){
  if (amass < 0.939 && amass > 0.937){
    ofile<<"!!! AMASS = "<<amass<<endl;      // only for debug purposes
    cout <<"!!! AMASS = "<<amass<<endl;      // -/-
    a62vec outvec;
    outvec.id1 = 1;
    outvec.id2 = 1;
    outvec.amass = amass;
    outvec.px = px;
    outvec.py = py;
    outvec.pz = pz;
  
    outvec.id1m = 888;
    outvec.id2m = 888;
    outvec.amassm = 0.0;
    outvec.pxm = 0.0;
    outvec.pym = 0.0;
    outvec.pzm = 0.0;
  
    ofile<<"B: "<<outvec.id1 <<" "<<outvec.id2 <<" "<<outvec.amass <<" "<<outvec.px <<" "<<outvec.py <<" "<<outvec.pz  <<endl;
    ofile<<"M: "<<outvec.id1m <<" "<<outvec.id2m <<" "<<outvec.amassm <<" "<<outvec.pxm <<" "<<outvec.pym <<" "<<outvec.pzm  <<endl; // there is nothing in this case
    return outvec;
  }
  
  double AMASSR, PXR, PYR, PZR;
  double AMASSf, PXf, PYf, PZf, SQS, S, EDEPR;
  double EENU, PPX, PPY, PPZ;
  double EEPI, PPIX, PPIY, PPIZ;
  double AMASSmf, PPI0f, PPIXf, PPIYf, PPIZf;
  double EETA;
  short Zmf, IZmf;
  a4vec vec;
  
  std::random_device rd;
  std::mt19937 gen(rd());
  
  AMASSR = amass;
  PXR = px;
  PYR = py;
  PZR = pz;
  
// ------- KINETIC VARIABLES FOR THE DELTA
  EDEPR  = sqrt(pow(AMASSR,2)+pow(PXR,2)+pow(PYR,2)+pow(PZR,2));
  
  SQS = AMASSR;
  S = pow(SQS,2);
//  ofile<< SQS <<" "<< AMASSR<<" " << S  <<endl;
  bool IANTI = false;
  if(id1 <= -1) IANTI = true;
  short ITYP=abs(id1);
  short IZ=id2;
  if(IANTI == true) IZ = -id2;

  
//------------------------------------------------------------
// ------- DECISION IF A N(1535) DECAYS INTO AN ETA OR A PION
//         WMASS IS SET TO EITHER MASS
  srand (time(NULL));
  double RNX = std::generate_canonical<double, 1>(gen);
  double WMASS = PMASS;
  if(ITYP == 4)
  { 
      if(RNX >= 0.55) WMASS = EMASS;
      else WMASS = PMASS;
  }
  
  double RRMASS = RMASS;
  
/*------------------------------------------------------------
* ------- DECISION IF A SIGMA* DECAYS INTO AN LAMBDA OR A SIGMA
*         RRMASS IS SET TO EITHER MASS
*/
  if(ITYP == 7)
  { 
    if(RNX > 0.88) RRMASS = 1.190;
    else RRMASS = 1.116;
  }
  

// ------------------------------------------------------------
//------- CHECK IF RESONANCE HAS ENOUGH ENERGY TO DECAY
  
//  if(SQS <= RRMASS+WMASS)     GOTO 800 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

// ------- THROWING DICE FOR THE DIRECTION OF DECAY IN DELTA REST FRAME
//
  double XX, YY, ZZ, RR = 0.0;
  while( (RR < 0.001) || (RR > 1.0) ){
    XX = 1. - 2. * std::generate_canonical<double, 1>(gen);
    YY = 1. - 2. * std::generate_canonical<double, 1>(gen);
    ZZ = 1. - 2. * std::generate_canonical<double, 1>(gen);
    RR = sqrt( pow(XX,2) + pow(YY,2) +pow(ZZ,2));
  }
  
double PX, PY, PZ, BEX, BEY, BEZ, ENU;
double PA, PA2, EPI;
/*------------------------------------------------------------
********************* N(1535) ->N + ETA  *******************
c------------------------------------------------------------
* --------- DECAY OF THE N(1535) INTO ETA IS CALCULATED
*           FLAG = 1 IF IT DECAYS
*/
  if(WMASS > PMASS+0.1)
  {
// --------- KINETIC VARIABLE FOR PRODUCED ETA AND NUCLEON ARE CALCULATED
    PA2 = 1.0 / (4*S)*(pow(S,2)+pow(RRMASS,4)+pow(EMASS,4) 
        - 2*S*pow(RRMASS,2)-2*pow(EMASS,2)*S  
        - 2*pow(EMASS,2)*pow(RRMASS,2));
    if(PA2 < 0) exit(0);
    PA = sqrt(PA2);
    PX = PA * XX / RR;
    PY = PA * YY / RR;
    PZ = PA * ZZ / RR;
    BEX = - PXR / EDEPR;
    BEY = - PYR / EDEPR;
    BEZ = - PZR / EDEPR;
    ENU = sqrt(pow(RRMASS,2) + PA2);

// --------- TRANSFORMATION INTO LAB FRAME
//..nucleon
    vec = lorentz(BEX,BEY,BEZ,ENU,PX,PY,PZ);
    EENU = vec.a0, PPX = vec.ax, PPY = vec.ay, PPZ = vec.az;
    EETA = sqrt(pow(EMASS,2) + PA2);
//..eta
    vec = lorentz(BEX,BEY,BEZ,EETA,-PX,-PY,-PZ);
    EEPI = vec.a0, PPIX = vec.ax, PPIY = vec.ay, PPIZ = vec.az;
    
/*------------------------------------------------------------
*****************  END of N(1535) ->N + ETA  ****************
c------------------------------------------------------------
*/
  }

  else{

/*------------------------------------------------------------
******************** Delta   -> N + Pi  **********************
******************** N(1440) -> N + Pi  **********************
******************** Sigma*  -> Lambda + Pi ******************
******************** Sigma*  -> Sigma  + Pi ******************
c------------------------------------------------------------
*/ 
    BEX    = - PXR / EDEPR;
    BEY    = - PYR / EDEPR;
    BEZ    = - PZR / EDEPR;
//    ofile<< S <<" "<< RRMASS<<" " << PMASS  <<endl;
    PA2 = 1.0 / (4*S)*(pow(S,2)+pow(RRMASS,4)+pow(PMASS,4)
        - 2*S*pow(RRMASS,2)-2*pow(PMASS,2)*S
        - 2*pow(PMASS,2)*pow(RRMASS,2));
    if(PA2 < 0) exit(0);
    PA=sqrt(PA2);
//    ofile<<" PA2 "<<PA2  <<endl;
//    ofile<<" PA "<<PA  <<endl;
//    ofile<<" RR "<<RR  <<endl;
    PX = PA * XX / RR;
    PY = PA * YY / RR;
    PZ = PA * ZZ / RR;
    
//    ofile<<PX <<" "<<PY <<" "<<PZ  <<endl;
    
    EPI =sqrt(PA2+pow(PMASS,2));
// --------- TRANSFORMATION INTO LAB FRAME
    ENU = sqrt(pow(RRMASS,2) + PA2 );
//..   nucleon:

    vec = lorentz(BEX,BEY,BEZ,ENU,-PX,-PY,-PZ);
    EENU = vec.a0, PPX = vec.ax, PPY = vec.ay, PPZ = vec.az;
//    ofile<<"lor: "<<PPX <<" "<<PPY <<" "<<PPZ  <<endl;
//..   pion:
    vec = lorentz(BEX,BEY,BEZ,EPI,PX,PY,PZ);
    EEPI = vec.a0, PPIX = vec.ax, PPIY = vec.ay, PPIZ = vec.az;
//------------------------------------------------------------
//**************** END D,N(1440) ->N + Pi *********************
  }

/*
c*******************************************************************
c.. momentum of nucleon: (eenu,ppx,ppy,ppz), mass=Rrmass
c.. momentum of the meson (pion or eta):
c..                      (eepi,ppix,ppiy,ppiz), mass=Wmass
c*******************************************************************
*/
// Final nucleon:
  PXf = PPX;
  PYf = PPY;
  PZf = PPZ;
  AMASSf = RRMASS;

// Final meson:
  PPIXf = PPIX;
  PPIYf = PPIY;
  PPIZf = PPIZ;
  PPI0f = sqrt(pow(WMASS,2)+pow(PPIX,2)+pow(PPIY,2)+pow(PPIZ,2));
  AMASSmf = WMASS;
  
/*c----------------------------------------------------------------------
* ------------- DEFINITON OF THE ISOSPIN OF OUTGGOING PARTICLES
*/
  short IDf = 1;   // set (preliminary) outgoing baryon = nucleon
  short IZf = IZ;  // set (preliminary) the N-charge = Resonance
  short IDmf =1;   // set (preliminary) output meson = pion

  XX = std::generate_canonical<double, 1>(gen);
  if(ITYP != 7) IDf = 1;
//****** c..delta -> n + pi ***********
  switch (ITYP) {
  case 2:
//C--

    switch (IZ) {
      case 2: //c..delta++ -> p + pi+
        IZf = 1;
        Zmf = 1;
      break;
      case -1: //c..   delta- -> n + pi-
        IZf  = 0;
        IZmf =-1;
      break;
      case 1: //c..delta+ -> n + pi+    WK=0.33333
              //c..delta+ -> p + pi0    WK=0.66666
        IZf=1;
        IZmf=0;
        if(XX > 0.66666667){
          IZf=0;
          IZmf=1;
        }
      break;
      case 0: //c..delta0 -> n + pi0    WK=0.66666
              //c..delta0 -> p + pi-    WK=0.33333
        IZf=0;
        IZmf=0;
        if(XX > 0.66666667){
          IZf=1;
          IZmf=-1;
        }
      break;
      default:
      break;
    }
   break;
//c*********** N(1440) -> N + pi ************************
  case 3:
      if(XX > 0.33333){
        IZmf = 2 * IZ - 1;
        IZf  = 1 - IZ;
      }
  break;  
//c*********** N(1535) -> N + pi/eta ************************
  case 4: 
      if(IDmf == 1 && XX > 0.33333)
      {
        IZmf = 2 * IZ - 1;
        IZf  = 1 - IZ;
      }
  break;
  case 7: 
//c*********** SIGMA* -> LAMBDA + pi ************************
    if(abs(RRMASS-1.1160) < 0.01)
    {
      IDf = 5;
      IZmf = IZ;
      IZf  = 0;
    }
//c*********** SIGMA* -> SIGMA + pi ************************
    else if(abs(RRMASS-1.190) < 0.01)
    {
      IDf = 6;
      RNX=std::generate_canonical<double, 1>(gen);
      switch (IZ) {
        case 2:
          IZmf = 1;
          IZf  = 0;
          if (RNX > 0.5) {IZmf = 0; IZf = 1;}
        break;
        case -1:
          IZmf = -1;
          IZf  = 0;
          if(RNX > 0.5) {IZmf = 0; IZf = -1;}
        break;
        case 0:
          IZmf = 1;
          IZf  = -1;
          if(RNX > 0.5) {IZmf = -1; IZf = 1;}
        break;
      }
    }
    break;
  }

  
  if(IANTI == true)
  {
    IDf=-IDf;
    IZf=-IZf;
    IZmf=-IZmf;
  }
 
  
  a62vec outvec;
  outvec.id1 = IDf;
  outvec.id2 = IZf;
  outvec.amass = AMASSf;
  outvec.px = PXf;
  outvec.py = PYf;
  outvec.pz = PZf;
  
  outvec.id1m = IDmf;
  outvec.id2m = IZmf;
  outvec.amassm = AMASSmf;
  outvec.pxm = PPIXf;
  outvec.pym = PPIYf;
  outvec.pzm = PPIZf;

  ofile<<"B: "<<outvec.id1 <<" "<<outvec.id2 <<" "<<outvec.amass <<" "<<outvec.px <<" "<<outvec.py <<" "<<outvec.pz  <<endl;
  ofile<<"M: "<<outvec.id1m <<" "<<outvec.id2m <<" "<<outvec.amassm <<" "<<outvec.pxm <<" "<<outvec.pym <<" "<<outvec.pzm  <<endl;
  return outvec;
}



int main(){
  ifstream fInputB; 
  ifstream fInputM; 
  const char* fFileB = "fort.305"; 
  const char* fFileM = "fort.306";
  
  cout << "-I : Opening input file(B) " << fFileB << endl;
  fInputB.open(fFileB, ios::in);
  if ( ! fInputB) { cout << "-E : Can not open input file " << fFileB << endl; exit(1); }
  
  cout << "-I : Opening input file(M) " << fFileM << endl;
  fInputM.open(fFileM, ios::in);
  if ( ! fInputM) { cout << "-E : Can not open input file " << fFileM << endl; exit(1); }
  
  
   
  short tA = 0, tZ = 0, pA = 0, pZ = 0;
  double pE = 0.0;
  fInputB>> tA >> tZ >> pA >> pZ >> pE;
  cout << pE*1000 << " MeV in lab frame "<<endl;

  short nSUB = 0, nRUN = 0;
  double BIMP = 0.0, tSACA = 0.0;
  fInputB>> nSUB >> nRUN >> BIMP >> tSACA;

  int nEVT = 0, NT = 0, SEED = 0;
  double DT = 0.0, WRTIME = 0.0;
  fInputB>> nEVT >> NT >> DT >> WRTIME >> SEED;

  double df = 0.0, VY = 0.0, arang = 0.0, alpha = 0.0, beta = 0.0, gamma = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0; // dummy
  
  short id1, id2, isub, irun;
  double px, py, pz, en, x, y, z, bimp, time;
  
  
  short Bid1M, Bid2M;
  double BpxM, BpyM, BpzM;
  
  
  short id1m, id2m, isubm, irunm;
  double pxm, pym, pzm, enm, xm, ym, zm, bimpm, timem;
  
  double obimp = 0.0, otime = 0.0;
  int oirun = 0;
  short pdt = -9999, npart = 0;
  double amass = 0.0;
  double amassm = 0.0;
  double BamassM = 0.0;
  short pdtm = -9999;
  
  
  short NtSACA=0;
  double HtArno=0.2;


	double epsilonX=0.01, epsilonY=0.01, epsilonZ=0.01 ;

  
  vector<short> vid1, vid2, visub, virun, vIDSACA;
  vector<double> vpx, vpy, vpz, ven, vx, vy, vz, vbimp, vtime;
  
  
  vector<short> vid1m, vid2m, visubm, virunm, vIDSACAm;
  vector<double> vpxm, vpym, vpzm, venm, vxm, vym, vzm, vbimpm, vtimem;
  
  
  a62vec outvec;
  
  short IDSACA, IDSACAm, BIDSACAM;
  bool is_bdec = false;
  FILE *f = fopen("saca_vk_BDEC.dat", "w");
  if (f == NULL) { printf("Error opening file!\n"); exit(1); }
  fprintf(f, "  %4d  %4d  %4d  %4d  %8.2f\n", tA, tZ, pA, pZ, pE*1000);
  fprintf(f, "  %6d  %4d  %6.2f  %8.2f  %16d\n", nSUB*nRUN*(NtSACA+1)*NUM_B, NtSACA+1, DT,tSACA, SEED);
  fprintf(f, "  %8.3f  %8.3f  %8.4f  %8.4f  %8.5f  %8.5f  %8.6f  %8.6f  %8.6f\n", df, VY, arang, alpha, beta, gamma, a1, a2, a3);
  
while(oirun != nSUB*nRUN*(NtSACA+1)*NUM_B) // set NUM_B to 1 for weighed B
{
  id1 = 0;
  id1m = 0;
  
  while ( id1 != 999 )
  { 
    fInputB>> id1 >> id2 >> isub >> irun >> px >> py >>  pz >> en >> x >> y >> z >> bimp >> time;
    if (id1 == 999) continue;
    if(pdt == -9999){ obimp = bimp; oirun++; pdt = round(round(time)/DT);}
    amass=sqrt(   pow(en, 2) - ( pow(px, 2) + pow(py, 2) + pow(pz, 2) )   );
    if (abs(id1) !=1 && id1 !=5 && id1 !=6 ){
      if( id1 > 7)id1 = 2;
      outvec = bdec(id1, id2, amass, px, py, pz);
      id1 = outvec.id1;
      id2 = outvec.id2;
      amass = outvec.amass;
      px = outvec.px;
      py = outvec.py;
      pz = outvec.pz;
      
      Bid1M = outvec.id1m;
      Bid2M = outvec.id2m;
      BamassM = outvec.amassm;
      BpxM = outvec.pxm;
      BpyM = outvec.pym;
      BpzM = outvec.pzm;
      
      if(Bid1M != 888 && Bid2M != 888) is_bdec = true;
    }
    IDSACA = particle_id(id1, id2, true); // baryons
    
    vid1.push_back(id1); vid2.push_back(id2); visub.push_back(isub); virun.push_back(irun); 
    vpx.push_back(px); vpy.push_back(py); vpz.push_back(pz); ven.push_back(amass); 
    vx.push_back(x); vy.push_back(y); vz.push_back(z); 
    vbimp.push_back(bimp); vtime.push_back(time); vIDSACA.push_back(IDSACA);
    
    if(is_bdec){
      BIDSACAM = particle_id(Bid1M, Bid2M , false); // mesons
      
      vid1.push_back(Bid1M); vid2.push_back(Bid2M); visub.push_back(isub); virun.push_back(irun); 
      vpx.push_back(BpxM); vpy.push_back(BpyM); vpz.push_back(BpzM); ven.push_back(BamassM); 
      vx.push_back(x+epsilonX); vy.push_back(y+epsilonY); vz.push_back(z+epsilonZ); 
      vbimp.push_back(bimp); vtime.push_back(time); vIDSACA.push_back(BIDSACAM);
      
      is_bdec = false;
    }
    
  }

  while ( id1m != 999 )  // !!! -------  MESONS
  { 
    fInputM>> id1m >> id2m >> isubm >> irunm >> pxm >> pym >>  pzm >> enm >> xm >> ym >> zm >> bimpm >> timem;
    if (id1m == 999) continue;
    amassm=sqrt(    pow(enm,2)   -   (  pow(pxm,2)+pow(pym,2)+pow(pzm,2)  )    );
    
    IDSACAm = particle_id(id1m, id2m, false); // mesons
    
    vid1m.push_back(id1m); vid2m.push_back(id2m); visubm.push_back(isubm); virunm.push_back(irunm); 
    vpxm.push_back(pxm); vpym.push_back(pym); vpzm.push_back(pzm); venm.push_back(amassm); 
    vxm.push_back(xm); vym.push_back(ym); vzm.push_back(zm); 
    vbimpm.push_back(bimpm); vtimem.push_back(timem); vIDSACAm.push_back(IDSACAm);
  }


  npart = vid1.size() + vid1m.size();
  fprintf(f, " %9.4f %15d %15d %15d\n", obimp, oirun, pdt, npart);
  pdt = -9999;
  
  for (int n = 0; n < vid1.size(); n++)
  {
    fprintf(f, " %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %3d %12.4E\n", vx[n], vy[n], vz[n], vpx[n], vpy[n], vpz[n], ven[n], vIDSACA[n], 1.);
  }
  for (int n = 0; n < vid1m.size(); n++)
  {
    fprintf(f, " %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %9.4f %3d %12.4E\n", vxm[n], vym[n], vzm[n], vpxm[n], vpym[n], vpzm[n], venm[n], vIDSACAm[n], 1.);
  }

   vid1.clear(); vid2.clear(); visub.clear(); virun.clear(); 
   vpx.clear(); vpy.clear(); vpz.clear(); ven.clear(); 
   vx.clear(); vy.clear(); vz.clear(); 
   vbimp.clear(); vtime.clear(); vIDSACA.clear();
   
   
   vid1m.clear(); vid2m.clear(); visubm.clear(); virunm.clear(); 
   vpxm.clear(); vpym.clear(); vpzm.clear(); venm.clear(); 
   vxm.clear(); vym.clear(); vzm.clear(); 
   vbimpm.clear(); vtimem.clear(); vIDSACAm.clear();
   
}

cout<<"DONE  "<< oirun <<endl;
  fclose(f);
  fInputB.close();
  fInputM.close();
  return 0;
}
